
from datetime import timedelta
from textwrap import dedent

# The DAG object; we'll need this to instantiate a DAG
from airflow import DAG

# Operators; we need this to operate!
from airflow.operators.bash import BashOperator
from airflow.utils.dates import days_ago
from airflow.operators.dummy import DummyOperator
from airflow.utils.task_group import TaskGroup
from airflow.operators.python import PythonOperator

import time



# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    # 'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': True,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}

def vorstrafen(ti):
    time.sleep(3)
    ti.xcom_push(key='vorstrafen', value=[1, 2, 4, 8])
    print('Vorstrafen print test')


def gebe_konto_frei(**kwargs):
    ti = kwargs['ti']

    pulled_value = ti.xcom_pull(key='vorstrafen', task_ids='vorstrafen')
    print(pulled_value)


with DAG(
    'a1_task_group_test',
    default_args=default_args,
    description='A simple tutorial DAG',
    schedule_interval="@once",
    start_date=days_ago(2),
    tags=['test'],
) as dag:
    start = DummyOperator(task_id="start")

    # [START howto_task_group_section_1]
    with TaskGroup("Kreditfreigabe_pruefen", tooltip="Prüft ob für ein Kredit würdig.") as kreditfreigabe:
        vorstrafen = PythonOperator(task_id="vorstrafen", python_callable=vorstrafen)
        gehalt = BashOperator(task_id="ermittle_gehalt", bash_command='echo 1')
        sicherheiten = DummyOperator(task_id="ermittle_sicherheiten")
        freigabe = PythonOperator(task_id="check_kredit", python_callable=gebe_konto_frei)


        [vorstrafen, gehalt, sicherheiten] >> freigabe
    # [END howto_task_group_section_1]

    # [START howto_task_group_section_2]
    with TaskGroup("Kreditkonditionen_pruefen", tooltip="Ermittelt die zugehörigen Kreditkonditionen.") as konditionen:
        schufa = DummyOperator(task_id="Schufa_pruefen")

        # [START howto_task_group_inner_section_2]
        with TaskGroup("Ermittle_Kreditscore", tooltip="Tasks for inner_section2") as inner_section_2:
            addresse = DummyOperator(task_id="ermittle_adresse")
            kreditscore = DummyOperator(task_id="ermittle_lokalen_kreditscore")

            addresse >> kreditscore
        # [END howto_task_group_inner_section_2]

    # [END howto_task_group_section_2]

    end = DummyOperator(task_id='end')

    start >> [kreditfreigabe, konditionen] >> end