
from datetime import timedelta
from textwrap import dedent

# The DAG object; we'll need this to instantiate a DAG
from airflow import DAG

# Operators; we need this to operate!
from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}

# Write to xcom using the xcom push statement
def xcom_write(ti):
    print("::first_function_execute")
    # context['ti'].xcom_push(key='mykey', value="first_function_execute says Hello ")
    ti.xcom_push(key='model_accuracy', value="Hello from function 1")

# Reading from xcom by specifying key and task_ids
def xcom_read(ti):
    print("::second_function_execute")
    # instance = context.get("ti").xcom_pull(key="mykey")
    instance = ti.xcom_pull(key='model_accuracy', task_ids=['write_to_xcom'])
    print("I am in second_function_execute got value :{} from Function 1  ".format(instance[0]))

# Write to xcom using the PythonOperator Return statement
def xcom_read_simple(ti):
    print("::first_function_simple_execute")
    return "Hello from function 1 simple"

# Read from xcom without specifying key
def xcom_write_simple(ti):
    print("::second_function_simple_execute")
    instance = ti.xcom_pull(task_ids=['write_to_xcom'])
    print(instance)


with DAG(
    'a1_xcom_example_test',
    default_args=default_args,
    description='A simple xcom test',
    schedule_interval=timedelta(days=1),
    start_date=days_ago(2),
    tags=['test'],
    catchup=False,
) as dag:

    # t1, t2 and t3 are examples of tasks created by instantiating operators

    t1 = PythonOperator(
        task_id='write_to_xcom',
        python_callable=xcom_write,
    )

    t2 = PythonOperator(
        task_id='read_from_xcom',
        python_callable=xcom_read,
    )

    t1 >> t2
