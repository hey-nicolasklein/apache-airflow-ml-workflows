
from datetime import timedelta
from textwrap import dedent

# The DAG object; we'll need this to instantiate a DAG
from airflow import DAG

# Operators; we need this to operate!
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': True,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}

# Test functions
def write():
    return 'cool value'

def read(ti):
    result = ti.xcom_pull(task_ids=['addresse'])
    print("I am in read and got value :{} as simple array".format(result))
    print("I am in read got value :{} as array on position 0".format(result[0]))

# Kreditfreigabe functions
def fetch_vorstrafen():
    return '3'

def fetch_gehalt():
    return '30.000'

def fetch_sicherheiten():
    return 'no'

def evaluate_freigabe(ti):
    vorstrafen = ti.xcom_pull(task_ids=['vorstrafen'])
    gehalt = ti.xcom_pull(task_ids=['gehalt'])
    sicherheiten = ti.xcom_pull(task_ids=['sicherheiten'])
    print("The person has {} previous convictions and earns {} euros per year. She has {} safeties."
          .format(vorstrafen[0], gehalt[0], sicherheiten[0]))
    print("The person is approved for a credit.")
    return True

# Kreditkonditionen Funktionen
def fetch_schufa():
    return 'good'

def fetch_addresse():
    return'253235235'

def fetch_creditscore(ti):
    person_address = ti.xcom_pull(task_ids=['addresse'])
    print("Person lives at {}."
          .format(person_address[0]))
    return '69'

def calculate_conditions(ti):
    schufa = ti.xcom_pull(task_ids=['schufa'])
    kreditscore = ti.xcom_pull(task_ids=['kreditscore'])
    print("Schufa reports {} for person. Local credit-score is {}"
          .format(schufa[0], kreditscore[0]))
    return '2.000 euros per month with 4% interest-ratio.'

# Credit offer

def credit_offer(ti):
    is_approved = ti.xcom_pull(task_ids=['freigabe'])
    conditons = ti.xcom_pull(task_ids=['konditionen'])
    if is_approved:
        print("Person is approved for a credit with condition {}".format(conditons[0]))
    else:
        print("Person is not approved for a credit")



with DAG(
    'a1_Kreditangebot_erstellen',
    default_args=default_args,
    description='A simple tutorial DAG',
    schedule_interval=timedelta(days=1),
    start_date=days_ago(2),
    tags=['seminar'],
) as dag:

    # Kreditfreigabe
    vorstrafen = PythonOperator(
        task_id='vorstrafen',
        python_callable=fetch_vorstrafen
    )

    gehalt = PythonOperator(
        task_id='gehalt',
        python_callable=fetch_gehalt
    )

    sicherheiten = PythonOperator(
        task_id='sicherheiten',
        python_callable=fetch_sicherheiten
    )

    freigabe = PythonOperator(
        task_id='freigabe',
        python_callable=evaluate_freigabe
    )

    # Kreditkonditionen

    addresse = PythonOperator(
        task_id='addresse',
        python_callable=fetch_addresse
    )

    kreditscore = PythonOperator(
        task_id='kreditscore',
        python_callable=fetch_creditscore
    )

    schufa = PythonOperator(
        task_id='schufa',
        python_callable=fetch_schufa
    )

    konditionen = PythonOperator(
        task_id='konditionen',
        python_callable=calculate_conditions
    )

    # Angebot erstellen
    angebot = PythonOperator(
        task_id='angebot',
        python_callable=credit_offer
    )

    [vorstrafen, gehalt, sicherheiten] >> freigabe >> angebot
    addresse >> kreditscore >> konditionen >> angebot
    schufa >> konditionen >> angebot