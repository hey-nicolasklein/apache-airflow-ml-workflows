
from datetime import timedelta
from textwrap import dedent

# The DAG object; we'll need this to instantiate a DAG
from airflow import DAG

# Operators; we need this to operate!
from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import linear_model, metrics


# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}


# Fetch data from REST-API and save it to the local DB.
def fetch_data(ti):
    pass


# Load the fetched data from the db and train a multiple regression model on it
# using 2 features to predict a third.
# Compute the corresponding MSE and publish it through xcom.
def train_model_regression(ti):
    # Autodaten einlesen
    cars = pd.read_csv('/opt/airflow/files/auto-mpg.data', header=None, sep='\s+')  # Verbrauchswerte extrahieren
    y = cars.iloc[:, 0].values
    # Leistungswerte extrahieren
    X = cars.iloc[:, [3]].values  # Plot erstellen
    # Einfache lineare Regression
    reg = linear_model.LinearRegression()
    reg.fit(X, y)
    # Koeffizienten und Bestimmtheitsmaß ausgeben
    print('Parameter:')
    print('w0: %f' % reg.intercept_)
    print('w1: %f' % reg.coef_[0])
    print('Bestimmtheitsmaß')
    print('R2: %f' % metrics.r2_score(y, reg.predict(X)))
    return


# Load the fetched data from the db and train a multiple regression model on it
# using 3 features to predict a fourth.
# Compute the corresponding MSE and publish it through xcom.
def train_model_regression_3_features(ti):
    pass


# Load the fetched data from the db and train a polynomial regression model on it
# using 1 features to predict a second.
# Compute the corresponding MSE and publish it through xcom.
def train_model_polynomial_regression(ti):
    pass


# Read MSE values from xcom and determine best model
def choose_model(ti):
    pass


with DAG(
    'a1_ML',
    default_args=default_args,
    description='Machine Learning Tests',
    schedule_interval=timedelta(days=1),
    start_date=days_ago(2),
    tags=['Project'],
    catchup=False,
) as dag:

    # Tasks created by instantiating operators

    fetchData = PythonOperator(
        task_id='fetch_data',
        python_callable=fetch_data,
    )

    regression2Features = PythonOperator(
        task_id='train_model_regression',
        python_callable=train_model_regression,
    )

    regression3Features = PythonOperator(
        task_id='train_model_regression_3_features',
        python_callable=train_model_regression_3_features,
    )

    polynomialRegression = PythonOperator(
        task_id='train_model_regression_polynomial_regression',
        python_callable=train_model_polynomial_regression,
    )

    chooseModel = PythonOperator(
        task_id='choose_model',
        python_callable=choose_model,
    )

    fetchData >> [regression2Features, regression3Features, polynomialRegression] >> chooseModel
