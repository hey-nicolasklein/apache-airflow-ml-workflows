# ML with Apache Airflow

## Getting Started

1. Run `docker build -t airflow_extended .` on the Dockerfile located in `./dockerfiles`
2. Now jump back to root with `cd ..`
3. Finally, you are all set to launch Apache Airflow using `docker compose up`.
